# OpenML dataset: analcatdata_negotiation

https://www.openml.org/d/491

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

analcatdata    A collection of data sets used in the book "Analyzing Categorical Data,"
by Jeffrey S. Simonoff, Springer-Verlag, New York, 2003. The submission
consists of a zip file containing two versions of each of 84 data sets,
plus this README file. Each data set is given in comma-delimited ASCII
(.csv) form, and Microsoft Excel (.xls) form.

NOTICE: These data sets may be used freely for scientific, educational and/or
noncommercial purposes, provided suitable acknowledgment is given (by citing
the above-named reference).

Further details concerning the book, including information on statistical software
(including sample S-PLUS/R and SAS code), are available at the web site

http://www.stern.nyu.edu/~jsimonof/AnalCatData


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: last


Note: Quotes, Single-Quotes and Backslashes were removed, Blanks replaced
with Underscores

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/491) of an [OpenML dataset](https://www.openml.org/d/491). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/491/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/491/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/491/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

